<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'Homecontroller@front');

Route::get('/form', 'AuthController@form');

Route::post('/post', 'AuthController@kirim');

Route::get('/table', function(){
    return view('tabel.table');
});

Route::get('/datatable', function(){
    return view('tabel.datatable');
  
});

route::get('/cast', 'castController@index');
route::get('/cast/create', 'castController@create');
route::post('/cast', 'castController@store');
route::get('/cast/{cast_id}', 'castController@show');
route::get('/cast/{cast_id}/edit', 'castController@edit');
route::put('/cast/{cast_id}', 'castController@update');
route::delete('/cast/{cast_id}', 'castController@destroy');