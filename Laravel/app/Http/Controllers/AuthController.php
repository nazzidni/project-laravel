<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('page.form');
    }
    public function kirim(Request $request){
        $firstname = $request->name;
        $lastname = $request->nama;
        return view('page.first', compact('firstname', 'lastname'));
    }
}
