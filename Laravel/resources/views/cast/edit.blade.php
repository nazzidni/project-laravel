@extends('layout.master')


@section('judul')
    edit cast
@endsection

@section('isi')

<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
        <label>nama cast</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan nama cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
        <label>umur cast</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan umur cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
        <label>bio</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Masukkan bio cast">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>     
</div>

@endsection