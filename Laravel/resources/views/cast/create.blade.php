@extends('layout.master')


@section('judul')
    Tambah Cast
@endsection

@section('isi')

<div>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
        <label>nama cast</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
        <label>umur cast</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan umur cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
        <label>bio</label>
                <input type="text" class="form-control" name="bio" placeholder="Masukkan bio cast">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>     
</div>

@endsection