@extends('layout.master')


@section('judul')
    Buat Account Baru
@endsection

@section('isi')
<h2>Sign Up Form</h2>
    <form action="/post" method="POST">
        @csrf
        <label> First Name </label><br>
        <input type="text" name="name"><br><br>

        <label> Last Name </label><br>
        <input type="text" name="nama"><br><br>

        <label >Gender: </label> <br>
        <input type="radio" name="JK" value="Male">Male <br>
        <input type="radio" name="JK" value="Female">Female <br>
        <input type="radio" name="JK" value="Other">Other <br><br> 

        <label> Nationality </label>
        <select name="Negara">
            <option value="Indoensian"> Indonesia </option>
            <option value="Singapore"> Singapore </option>
            <option value="Malaysian"> Malaysian </option>
            <option value="Australian"> Australian </option>
        </select> <br><br>

        <label> Language Spoken </label> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Arabic <br>
        <input type="checkbox"> Japanese<br><br>
        
        </select>
        <label> Bio </label> <br>
        <textarea name="Bio" id="" cols="50" rows="10"></textarea> <br><br>
        
        <input type="submit" value="Sign Up">
    </form>

@endsection
    